{
    id:"diableavionics",
    "color":[119,52,47,255],
    "displayName":"Diable Avionics",
    "displayNameWithArticle":"the Diable Corporation",	
    "logo":"graphics/daflag.png",	
    "shipNamePrefix":"USN",	
    "shipNameSources":{
        "diableavionics":1,
    },	
    "names":{
        "modern":1,
    },
    "illegalCommodities":[
    ],
    "internalComms":"diableavionics_internal",	

    #"tariffFraction":0.13,
    #"tollFraction":0,
    #"fineFraction":0.5,		

    "portraits":{
        "standard_male":[
            "graphics/portraits/portrait_hegemony05.png",
            "graphics/portraits/portrait_mercenary01.png",
            "graphics/portraits/portrait18.png",
        ],
        "standard_female":[
            "graphics/portraits/portrait_hegemony04.png",
            "graphics/portraits/portrait16.png",
        ],
    },

    "custom":{
        "investigatesPlayerForGoodRepWithOtherFactions":true,
        "worthInvestigatingPlayerForGoodRepWith":true,
        "engageWhenEvenStrength":true,
        "offersCommissions":true,
        "engagesInHostilities":true,
},
    "shipRoles":{
        "interceptor":{
            "diableavionics_strife_wing":10,
            "diableavionics_warlust_wing":2,
            "diableavionics_frost_wing":2,
            "diableavionics_valiant_wing":4,
            "fallback":{"fighter":1},
        },
        "fighter":{
            "diableavionics_frost_wing":5,
            "diableavionics_warlust_wing":8,
            "diableavionics_strife_wing":10,
            "diableavionics_valiant_wing":3,	
            "diableavionics_raven_wing":1,	
            "fallback":{"interceptor":1},
        },
        "bomber":{
            "diableavionics_blizzaia_wing":10,
            "diableavionics_raven_wing":2,
            "fallback":{"fighter":1},
        },

        "fastAttack":{
            "diableavionics_vapor_attack":7,
            "diableavionics_vapor_standard":7,
            "diableavionics_vapor_closeRange":7,
            "diableavionics_vapor_closequarter":7,
            "diableavionics_vapor_brawler":7,
            "diableavionics_raven_wing":3,
            "diableavionics_hayle_attack":2,
            "diableavionics_hayle_multirole":2,
            "diableavionics_versant_standard":2,
            "fallback":{"combatSmall":1},
        },

        "escortSmall":{
            "diableavionics_frost_wing":5,
            "diableavionics_draft_standard":10,
            "diableavionics_draft_support":5,
            "diableavionics_calm_standard":2,
            "diableavionics_calm_closequarter":2,
            "diableavionics_versant_escort":2,
            "diableavionics_vapor_closequarter":5,
            "fallback":{"combatSmall":3},
        },
        "escortMedium":{
            "diableavionics_calm_standard":5,
            "diableavionics_calm_assault":5,
            "diableavionics_calm_brute":5,	
            "diableavionics_calm_mixed":5,	
            "diableavionics_calm_closequarter":5,
            "diableavionics_hayle_closequarter":5,
            "diableavionics_gust_standard":2,	
            "fallback":{"combatMedium":3},
        },

        "combatSmall":{
            "diableavionics_draft_standard":5,
            "diableavionics_draft_support":5,
            "diableavionics_draft_assault":5,
            "diableavionics_vapor_standard":10,
            "diableavionics_vapor_attack":10,
            "diableavionics_vapor_closeRange":10,
            "diableavionics_vapor_brawler":10,
            "diableavionics_hayle_standard":2,
            "diableavionics_versant_assault":2,
            "fallback":{"escortSmall":2},
        },

        "combatMedium":{
            "diableavionics_calm_standard":5,
            "diableavionics_calm_assault":5,
            "diableavionics_calm_brute":5,	
            "diableavionics_calm_closequarter":5,
            "diableavionics_hayle_standard":5,
            "diableavionics_hayle_attack":5,	
            "diableavionics_hayle_combat":5,	
            "diableavionics_hayle_multirole":5,	
            "diableavionics_hayle_closequarter":5,
            "diableavionics_derecho_support":5,	
            "diableavionics_derecho_standard":5,
            "diableavionics_derecho_kite":5,	
            "diableavionics_haze_standard":2,	
            "fallback":{"combatSmall":3},
        },

        "combatLarge":{
            "diableavionics_haze_standard":5,	
            "diableavionics_haze_assault":5,
            "diableavionics_haze_missile":5,
            "diableavionics_haze_support":5,
            "diableavionics_haze_closequarter":5,	
            "diableavionics_gust_assault":5,	
            "diableavionics_gust_sentinel":5,
            "diableavionics_gust_standard":5,	
            "diableavionics_gust_missile":5,
            "fallback":{"combatMedium":2},
        },

        "combatCapital":{
            "diableavionics_pandemonium_willBreaker":1,	
            "diableavionics_pandemonium_bonesShatterer":1,
            "diableavionics_pandemonium_gutsRipper":1,	
            "diableavionics_pandemonium_mindBlower":1,
            "diableavionics_maelstrom_standard":5,			
            "diableavionics_maelstrom_beamer":5,			
            "diableavionics_maelstrom_brawler":5,		
            "diableavionics_maelstrom_vanguard":5,			
            "diableavionics_storm_standard":2,	
            "diableavionics_storm_assault":2,	
            "diableavionics_storm_support":2,	
            "diableavionics_storm_closequarter":2,	
            "fallback":{"combatLarge":2},
        },


        # hybrid ships with good combat and cargo capacity
        "combatFreighterSmall":{
            "diableavionics_draft_standard":10,
            "diableavionics_draft_support":5,
            "fallback":{"freighterSmall":1},
        },
        "combatFreighterMedium":{
            "diableavionics_chinook_support":5,
            "diableavionics_stratus_standard":10,
            "diableavionics_stratus_combat":10,
            "fallback":{"combatFreighterSmall":2},
        },
        "combatFreighterLarge":{
            "diableavionics_rime_breach":10,
            "diableavionics_rime_support":10,
            "fallback":{"combatFreighterMedium":2},
        },

        "civilianRandom":{
            "diableavionics_laminar_miner":10,
            "fallback":{"freighterSmall":1},
        },

        # carriers
        "carrierSmall":{
            "diableavionics_fractus_support":10,
            "diableavionics_fractus_attack":10,
            "diableavionics_fractus_standard":10,
            "fallback":{"carrierMedium":1},
        },
        "carrierMedium":{				
            "diableavionics_gust_standard":10,
            "diableavionics_gust_sentinel":10,
            "diableavionics_gust_assault":10,
            "diableavionics_gust_missile":10,
            "fallback":{"carrierSmall":2},
        },
        "carrierLarge":{				
            "diableavionics_storm_standard":5,	
            "diableavionics_storm_assault":5,	
            "diableavionics_storm_support":5,	
            "diableavionics_storm_closequarter":5,
            "diableavionics_maelstrom_standard":1,			
            "diableavionics_maelstrom_beamer":1,			
            "diableavionics_maelstrom_brawler":1,		
            "diableavionics_maelstrom_vanguard":1,	
            "diableavionics_pandemonium_willBreaker":1,	
            "diableavionics_pandemonium_bonesShatterer":1,
            "diableavionics_pandemonium_gutsRipper":1,	
            "diableavionics_pandemonium_mindBlower":1,
            "fallback":{"carrierMedium":2},
        },

        # freighters and such
        "freighterSmall":{
            "fallback":{"freighterMedium":1},
        },
        "freighterMedium":{
            "diableavionics_stratus_standard":10,
            "diableavionics_stratus_combat":10,
            "fallback":{"freighterSmall":2},
        },
        "freighterLarge":{
            "diableavionics_rime_standard":10,
            "diableavionics_rime_support":4,
            "diableavionics_rime_breach":4,
            "fallback":{"freighterMedium":2},
        },

        "tankerSmall":{
            "fallback":{"tankerMedium":1},
        },
        "tankerMedium":{
            "diableavionics_chinook_standard":10,
            "fallback":{"tankerSmall":2},
        },
        "tankerLarge":{
            "fallback":{"tankerMedium":2},
        },

        "personnelSmall":{
            "fallback":{"personnelMedium":1},
        },
        "personnelMedium":{
            "diableavionics_cirrus_standard":10,
            "fallback":{"personnelSmall":2},
        },
        "personnelLarge":{
            "fallback":{"personnelMedium":2},
        },

	"linerSmall":{
            "fallback":{"linerMedium":0.5},
	},
	"linerMedium":{
            "diableavionics_cirrus_standard":10,
            "fallback":{"linerSmall":2},
	},
	"linerLarge":{
            "fallback":{"linerMedium":2},
	},

        # utility ships
        "tug":{
            "diableavionics_shear_standard":10,
            "fallback":{"utility":1},
        },
        "crig":{
            "crig_Standard":10,
            "fallback":{"utility":1},
        },
        "utility":{
            "diableavionics_laminar_miner":10,
            "crig_Standard":10,
            "diableavionics_shear_standard":10,
        },
    },		
    "traits":{
        "admiral":{         
        },
        "captain":{
            "timid":1,
            "cautious":5,
            "steady":10,
            "aggressive":7,
            },
        }, 
    "doctrine":{
        # ship distribution
        "interceptors":10,
        "fighters":10,
        "bombers":7,
        "small":5,
        "fast":5,
        "medium":5,
        "large":6,
        "capital":1,

        # as fraction of combat ships of same size
        "escortSmallFraction":1,
        "escortMediumFraction":1,

        # freighters tend to be dedicated to role
        "combatFreighterProbability":0.25,

        # tend toward larger ships
        "minPointsForCombatCapital":15,
        "minPointsForLargeCarrier":10,

        # increased preference for carriers
        "smallCarrierProbability":0.4,
        "mediumCarrierProbability":0.5,
        "largeCarrierProbability":0.6,

        # officers ; TriTachyon is somewhat elite
        "officersPerPoint":0.45,
        "officerLevelPerPoint":0.4,
        "officerLevelBase":10,
        "officerLevelVariance":1,
    },	
},