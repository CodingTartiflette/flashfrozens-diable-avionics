package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.fleet.FleetMemberType;

/**
 *
 * credits to uomoz for sample and Mshadowy, and Tartiflette
 */
public class Diableavionics_stagging {
    
    public void generate(SectorAPI sector) {
            LocationAPI hyperlocation = Global.getSector().getHyperspace();
            
            SectorEntityToken DAstation = hyperlocation.addCustomEntity("diableavionics_eclipse","Staging Point Eclipse", "diableavionics_station_eclipse", "diableavionics");	
            DAstation.setCustomDescriptionId("diableavionics_station_eclipse");
	    DAstation.setFixedLocation(21000, -1000);
                

                
                
            CargoAPI DAcargo = DAstation.getCargo();

            DAcargo.addCrew(CargoAPI.CrewXPLevel.VETERAN, 100);
            DAcargo.addCrew(CargoAPI.CrewXPLevel.REGULAR, 500);
            DAcargo.addSupplies(1250);
            DAcargo.addFuel(750);

            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_draft_Hull", null);                
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_draft_Hull", null);
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_vapor_Hull", null);      
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_vapor_Hull", null);                    
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_draft_Hull", null);                
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_calm_Hull", null);     
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_hayle_Hull", null);                    
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_gust_Hull", null);    
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_haze_Hull", null);                    
            DAcargo.addMothballedShip(FleetMemberType.SHIP, "diableavionics_storm_Hull", null);                 
            DAcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "diableavionics_warlust_wing", null);		
            DAcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "diableavionics_frost_wing", null);	
            DAcargo.addMothballedShip(FleetMemberType.FIGHTER_WING, "diableavionics_strife_wing", null);         

            DAcargo.addWeapons("diableavionics_ibis", 10);
            DAcargo.addWeapons("diableavionics_singlerecson", 6);                   
            DAcargo.addWeapons("diableavionics_thunderboltsmall", 6);   
            DAcargo.addWeapons("diableavionics_micromissile", 20);                
            DAcargo.addWeapons("diableavionics_recson", 6);
            DAcargo.addWeapons("diableavionics_thunderboltpod", 2);   
            DAcargo.addWeapons("diableavionics_plover", 2);                
            DAcargo.addWeapons("diableavionics_gravemg", 4);	
            DAcargo.addWeapons("diableavionics_state", 4);   
                
            SectorEntityToken DAfob = hyperlocation.addCustomEntity("diableavionics_shadow","Forward Operation Base", "diableavionics_station_shadow", "diableavionics");	
            DAfob.setCustomDescriptionId("diableavionics_station_shadow");
	    DAfob.setFixedLocation(13000, 13000);
            
            CargoAPI DAcargo2 = DAfob.getCargo();

            DAcargo2.addCrew(CargoAPI.CrewXPLevel.VETERAN, 50);
            DAcargo2.addCrew(CargoAPI.CrewXPLevel.REGULAR, 200);
            DAcargo2.addSupplies(750);
            DAcargo2.addFuel(500);

            DAcargo2.addMothballedShip(FleetMemberType.SHIP, "diableavionics_draft_Hull", null);                
            DAcargo2.addMothballedShip(FleetMemberType.SHIP, "diableavionics_draft_Hull", null);
            DAcargo2.addMothballedShip(FleetMemberType.SHIP, "diableavionics_vapor_Hull", null);      
            DAcargo2.addMothballedShip(FleetMemberType.SHIP, "diableavionics_vapor_Hull", null);                
            DAcargo2.addMothballedShip(FleetMemberType.SHIP, "diableavionics_calm_Hull", null);                   
            DAcargo2.addMothballedShip(FleetMemberType.FIGHTER_WING, "diableavionics_warlust_wing", null);		
            DAcargo2.addMothballedShip(FleetMemberType.FIGHTER_WING, "diableavionics_frost_wing", null);	
            DAcargo2.addMothballedShip(FleetMemberType.FIGHTER_WING, "diableavionics_strife_wing", null);         

            DAcargo2.addWeapons("diableavionics_ibis", 10);
            DAcargo2.addWeapons("diableavionics_singlerecson", 6);                   
            DAcargo2.addWeapons("diableavionics_thunderboltsmall", 6);   
            DAcargo2.addWeapons("diableavionics_micromissile", 20);                
            DAcargo2.addWeapons("diableavionics_recson", 6);
            DAcargo2.addWeapons("diableavionics_thunderboltpod", 2);   
            DAcargo2.addWeapons("diableavionics_plover", 2);                
            DAcargo2.addWeapons("diableavionics_gravemg", 4);	
            DAcargo2.addWeapons("diableavionics_state", 4);   
	}
}
