package data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.OrbitalStationAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;

import data.scripts.world.BaseSpawnPoint;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.lazywizard.lazylib.MathUtils;

public class Diableavionics_spawnPoint extends BaseSpawnPoint {

	public Diableavionics_spawnPoint(SectorAPI sector, LocationAPI location, 
							float daysInterval, int maxFleets, SectorEntityToken anchor) {
		super(sector, location, daysInterval, maxFleets, anchor);
	}

		protected CampaignFleetAPI spawnFleet() {
		//if ((float) Math.random() < 0.5f) return null;
		
		String type = null;
		float r = (float) Math.random();
		if (r > 0.5f) {			//	50% (50)
			type = "diableavionicsScout";
		} else if (r > 0.18f) {		//	32% (35)
			type = "diableavionicsEscort";		
		} else if (r > 0.08f) {		//	10% (35)
			type = "diableavionicsFleet";		                                
		} else  {					//	8%
			type = "diableavionicsDefenceFleet";
		}  
                
                
	return spawn(type);
	}
	
    private static StarSystemAPI getRandomSystem()
    {
        List<StarSystemAPI> systems = Global.getSector().getStarSystems();
        return systems.get(MathUtils.getRandom().nextInt(systems.size()));
    }                

    private static OrbitalStationAPI getRandomStationInSystem(StarSystemAPI system)
    {
        List<OrbitalStationAPI> stations = new ArrayList();
        Iterator<SectorEntityToken> iter = system.getOrbitalStations().iterator();
        while (iter.hasNext())
        {
            SectorEntityToken token = iter.next();
            if (!(token instanceof OrbitalStationAPI))
            {
                continue;
            }

            OrbitalStationAPI station = (OrbitalStationAPI) token;

            // Only attack enemy stations
            if (station.getFaction().isNeutralFaction() || station.getFaction().getRelationship("exipirated") >= 0f)
            {
                continue;
            }

            stations.add(station);
        }

        if (stations.isEmpty())
        {
            return null;
        }

        return stations.get(MathUtils.getRandom().nextInt(stations.size()));
    }    
    
	public CampaignFleetAPI spawn(String type) {		
		CampaignFleetAPI fleet = getSector().createFleet("diableavionics", type);
		getLocation().spawnFleet(getAnchor(), 0, 0, fleet);
		fleet.setPreferredResupplyLocation(getAnchor());
		
		StarSystemAPI askonia = Global.getSector().getStarSystem("Askonia");
		if (type.equals("diableavionicScout") || type.equals("diableavionicsEscort") ||type.equals("diableavionicsFleet") || (float) Math.random() < 0.5f) {
			if ((float) Math.random() > 0.30f) {
				if ((float) Math.random() > 0.5f) {
					fleet.addAssignment(FleetAssignment.RAID_SYSTEM, askonia.getHyperspaceAnchor(), 30);
					fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
				} else {
					fleet.addAssignment(FleetAssignment.RAID_SYSTEM, askonia.getStar(), 30);
					fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
				}
			} else {
				if ((float) Math.random() > 0.5f) {
                                    
                                        StarSystemAPI system = getRandomSystem();
                                        SectorEntityToken target = getRandomStationInSystem(system);
                                        
					fleet.addAssignment(FleetAssignment.RAID_SYSTEM, target, 30);
					fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
				} else {
					fleet.addAssignment(FleetAssignment.RAID_SYSTEM, null, 10);
					fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
				}
			}
		} else {
			fleet.addAssignment(FleetAssignment.DEFEND_LOCATION, getAnchor(), 20);
			fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, getAnchor(), 1000);
		}
		
		return fleet;
	}

}

