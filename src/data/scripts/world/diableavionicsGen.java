package data.scripts.world;

import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import data.scripts.world.systems.Diableavionics_stagging;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.shared.SharedData;
import data.scripts.world.systems.Diableavionics_outerTerminus;

@SuppressWarnings("unchecked")
public class diableavionicsGen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {
	
        new Diableavionics_stagging().generate(sector);
        new Diableavionics_outerTerminus().generate(sector);

        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("diableavionics");                      

        FactionAPI diableavionics = sector.getFaction("diableavionics");
        FactionAPI player = sector.getFaction(Factions.PLAYER);
        FactionAPI hegemony = sector.getFaction(Factions.HEGEMONY);
        FactionAPI tritachyon = sector.getFaction(Factions.TRITACHYON);
        FactionAPI pirates = sector.getFaction(Factions.PIRATES);
        FactionAPI independent = sector.getFaction(Factions.INDEPENDENT); 
        FactionAPI church = sector.getFaction(Factions.LUDDIC_CHURCH);
        FactionAPI path = sector.getFaction(Factions.LUDDIC_PATH);   	
        FactionAPI diktat = sector.getFaction(Factions.DIKTAT); 

        diableavionics.setRelationship(player.getId(), -0.19f);	
        diableavionics.setRelationship(hegemony.getId(), -0.51f);
        diableavionics.setRelationship(tritachyon.getId(), -0.65f);
        diableavionics.setRelationship(pirates.getId(), -0.2f);
        diableavionics.setRelationship(independent.getId(), -0.1f);	
        diableavionics.setRelationship(church.getId(), -0.4f);
        diableavionics.setRelationship(path.getId(), -0.70f);    
        diableavionics.setRelationship(diktat.getId(), -0.45f);                 
        diableavionics.setRelationship("exigency", -0.50f);	       
        diableavionics.setRelationship("shadow_industry", -0.55f);	         
        diableavionics.setRelationship("mayorate", 0.1f);	              
        diableavionics.setRelationship("blackrock", -0.75f);	    
        diableavionics.setRelationship("tiandong", 0.15f);     
        diableavionics.setRelationship("citadel", 0.2f);     
        diableavionics.setRelationship("SCY", 0.2f);     
        diableavionics.setRelationship("neutrinocorp", -0.15f); 
        diableavionics.setRelationship("interstellarimperium", -0.25f); 
    }
}
