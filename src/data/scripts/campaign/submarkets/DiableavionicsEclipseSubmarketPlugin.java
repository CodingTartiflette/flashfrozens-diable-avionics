package data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoAPI.CargoItemType;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.CoreUIAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.impl.campaign.submarkets.BaseSubmarketPlugin;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.Highlights;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.apache.log4j.Logger;


//Modified from Mesotronik's original plugin

public class DiableavionicsEclipseSubmarketPlugin extends BaseSubmarketPlugin
{
    public static Logger log = Global.getLogger(DiableavionicsEclipseSubmarketPlugin.class);

    @Override
    public String getIllegalTransferText(CargoStackAPI stack, TransferAction action)
    {
        RepLevel req = getRequiredLevelAssumingLegal(stack, action);

        if (req != null)
        {
            return "Requires: " + submarket.getFaction().getDisplayName() + " - " + req.getDisplayName().toLowerCase();
        }

        return "Illegal to trade in " + stack.getDisplayName() + " here";
    }

    @Override
    public String getIllegalTransferText(FleetMemberAPI member, TransferAction action)
    {
        RepLevel req = getRequiredLevelAssumingLegal(member, action);

        if (req != null)
        {
            return "Req: " + submarket.getFaction().getDisplayName() + " - " + req.getDisplayName().toLowerCase();
        }

        return "Illegal to buy or sell here";
    }

    @Override
    public float getTariff()
    {
        return 0f;
    }

    @Override
    public Highlights getTooltipAppendixHighlights(CoreUIAPI ui)
    {
        String appendix = getTooltipAppendix(ui);
        if (appendix == null)
        {
            return null;
        }

        Highlights h = new Highlights();
        h.setText(appendix);
        h.setColors(Misc.getNegativeHighlightColor());
        return h;
    }

    @Override
    public void init(SubmarketAPI submarket)
    {
        super.init(submarket);
    }

    @Override
    public boolean isBlackMarket()
    {
        return true;
    }

    @Override
    public boolean isIllegalOnSubmarket(String commodityId, TransferAction action)
    {
        return false;
    }

    @Override
    public boolean isIllegalOnSubmarket(CargoStackAPI stack, TransferAction action)
    {
        RepLevel req = getRequiredLevelAssumingLegal(stack, action);
        if (req == null)
        {
            return false;
        }

        RepLevel level = submarket.getFaction().getRelationshipLevel(Global.getSector().getFaction(Factions.PLAYER));

        boolean legal = level.isAtWorst(req);
        return !legal;
    }

    @Override
    public boolean isIllegalOnSubmarket(FleetMemberAPI member, TransferAction action)
    {
        RepLevel req = getRequiredLevelAssumingLegal(member, action);
        if (req == null)
        {
            return false;
        }

        RepLevel level = submarket.getFaction().getRelationshipLevel(Global.getSector().getFaction(Factions.PLAYER));

        boolean legal = level.isAtWorst(req);
        return !legal;
    }

    @Override
    public void updateCargoPrePlayerInteraction()
    {
        if (!okToUpdateCargo())
        {
            return;
        }
        sinceLastCargoUpdate = 0f;

        CargoAPI cargo = getCargo();
        float stability = market.getStabilityValue();
        float blackMarketLegalFraction = 0.1f - 0.01f * stability;
        float blackMarketIllegalFraction = 0.2f - 0.01f * stability;

        for (CommodityOnMarketAPI com : market.getAllCommodities())
        {
            String id = com.getId();
            boolean illegal = market.isIllegal(id);

            float desired;
            if (illegal)
            {
                desired = com.getAverageStockpileAfterDemand() * blackMarketIllegalFraction;
            }
            else
            {
                desired = com.getAverageStockpileAfterDemand() * blackMarketLegalFraction;
            }

            desired = (int) desired;

            switch (id)
            {
                case Commodities.FUEL:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinFuel"));
                    break;
                case Commodities.SUPPLIES:
                    desired = Math.max(desired, Global.getSettings().getFloat("blackMarketMinSupplies"));
                    break;
            }

            float current = cargo.getQuantity(CargoItemType.RESOURCES, id);
            if (desired > current)
            {
                cargo.addItems(CargoItemType.RESOURCES, id, (int) (desired - current));
            }
            else if (current > desired)
            {
                cargo.removeItems(CargoItemType.RESOURCES, id, (int) (current - desired));
            }
        }

        pruneWeapons(0.5f);

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        factionPicker.add(market.getFaction(), 10f - stability);
        factionPicker.add(submarket.getFaction(), 10f);
        factionPicker.add(Global.getSector().getFaction("diableavionics"), 2.5f);
        addWeaponsBasedOnMarketSize(4, 3, factionPicker);

        addRandomWeapons(Math.max(1, market.getSize() - 3), 3);

        addShips();
        cargo.sort();
    }

    private void addShips()
    {
        int marketSize = market.getSize();

        pruneShips(0.75f);

        WeightedRandomPicker<String> rolePicker = new WeightedRandomPicker<>();
        rolePicker.add(ShipRoles.CIV_RANDOM, 5f);
        rolePicker.add(ShipRoles.FREIGHTER_SMALL, 3f);
        rolePicker.add(ShipRoles.TANKER_SMALL, 1f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_SMALL, 15f);
        rolePicker.add(ShipRoles.COMBAT_SMALL, 15f);
        rolePicker.add(ShipRoles.COMBAT_MEDIUM, 15f);
        rolePicker.add(ShipRoles.INTERCEPTOR, 5f);
        rolePicker.add(ShipRoles.FIGHTER, 10f);
        rolePicker.add(ShipRoles.BOMBER, 7.5f);

        rolePicker.add(ShipRoles.FREIGHTER_MEDIUM, 5f);
        rolePicker.add(ShipRoles.TANKER_MEDIUM, 1f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM, 10f);
        rolePicker.add(ShipRoles.CARRIER_SMALL, 10f);

        rolePicker.add(ShipRoles.FREIGHTER_LARGE, 3f);
        rolePicker.add(ShipRoles.TANKER_LARGE, 1f);
        rolePicker.add(ShipRoles.COMBAT_FREIGHTER_LARGE, 5f);
        rolePicker.add(ShipRoles.COMBAT_LARGE, 5f);
        rolePicker.add(ShipRoles.CARRIER_MEDIUM, 10f);

        rolePicker.add(ShipRoles.COMBAT_CAPITAL, 3f);

        WeightedRandomPicker<FactionAPI> factionPicker = new WeightedRandomPicker<>();
        float stability = market.getStabilityValue();
        factionPicker.add(market.getFaction(), 10f - stability);
        factionPicker.add(submarket.getFaction(), 10f);
        factionPicker.add(Global.getSector().getFaction("diableavionics"), 2.5f);

        addShipsForRoles(marketSize * 3, rolePicker, factionPicker);
    }

    private RepLevel getRequiredLevelAssumingLegal(CargoStackAPI stack, TransferAction action)
    {
        if (stack.isWeaponStack())
        {
            if (action == TransferAction.PLAYER_BUY)
            {
                WeaponSpecAPI spec = stack.getWeaponSpecIfWeapon();
                if (!spec.getWeaponId().startsWith("diableavionics_"))
                {
                    return null;
                }

                int tier = spec.getTier();
                switch (tier)
                {
                    case 0:
                        return null;
                    case 1:
                        return RepLevel.SUSPICIOUS;
                    case 2:
                        return RepLevel.FAVORABLE;
                    case 3:
                        return RepLevel.FRIENDLY;
                }
            }
            return null;
        }

        return null;
    }

    private RepLevel getRequiredLevelAssumingLegal(FleetMemberAPI member, TransferAction action)
    {
        if (action == TransferAction.PLAYER_BUY)
        {
            int fp = member.getFleetPointCost();
            HullSize size = member.getHullSpec().getHullSize();
            if (!member.getHullId().startsWith("diableavionics_"))
            {
                return null;
            }

            if (size == HullSize.CAPITAL_SHIP || fp > 17)
            {
                if (member.getHullId().startsWith("diableavionics_"))
                {
                    return RepLevel.COOPERATIVE;
                }
                else
                {
                    return RepLevel.FRIENDLY;
                }
            }
            if (size == HullSize.CRUISER || fp > 10)
            {
                if (member.getHullId().startsWith("diableavionics_"))
                {
                    return RepLevel.FRIENDLY;
                }
                else
                {
                    return RepLevel.WELCOMING;
                }
            }
            if (size == HullSize.DESTROYER || fp > 6)
            {
                if (member.getHullId().startsWith("diableavionics_"))
                {
                    return RepLevel.WELCOMING;
                }
                else
                {
                    return RepLevel.FAVORABLE;
                }
            }
            if (member.getHullId().startsWith("diableavionics_"))
            {
                return RepLevel.FAVORABLE;
            }
            else
            {
                return RepLevel.NEUTRAL;
            }
        }
        return null;
    }
}