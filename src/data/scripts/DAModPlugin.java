package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import data.scripts.world.diableavionicsGen;

import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.weapons.Diableavionics_antiMissileAI;
import data.scripts.weapons.Diableavionics_banishAI;
import data.scripts.weapons.Diableavionics_thrushAI;

import data.scripts.weapons.diableavionics_ScatterMissileAI;
import data.scripts.weapons.diableavionics_SrabAI;
import data.scripts.weapons.diableavionics_ThunderboltMissileAI;
import data.scripts.weapons.diableavionics_swarmerMissileAI;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
//import org.dark.shaders.util.TextureData;

public class DAModPlugin extends BaseModPlugin {

    public static final String SCATTER_MISSILE_ID = "diableavionics_micromissile";    
    public static final String PD_MISSILE_ID = "diableavionics_magicmissile";
    public static final String THUNDERBOLT_MISSILE_ID = "diableavionics_thunderbolt";
    public static final String VALIANT_MISSILE_ID = "diableavionics_itanomissile";
    public static final String BANISH_ID = "diableavionics_banishmirv";
    public static final String THRUSH_ID = "diableavionics_thrushmirv";
    public static final String SRAB_ID = "diableavionics_srab_shot";
    
    private static void initDA()
    {
        try {  
            //Got Exerelin, so load Exerelin  
            Class<?> def = Global.getSettings().getScriptClassLoader().loadClass("exerelin.campaign.SectorManager");  
            Method method;  
            try {  
                method = def.getMethod("getCorvusMode");  
                Object result = method.invoke(def);  
                if ((boolean)result == true)  
                {  
                    // Exerelin running in Corvus mode, go ahead and generate our sector  
                new diableavionicsGen().generate(Global.getSector());
                }  
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException |  
                     InvocationTargetException ex) {  
                // check failed, do nothing  
            }  
        }
	catch (ClassNotFoundException ex) {
            new diableavionicsGen().generate(Global.getSector());
            // Exerelin not found so continue and run normal generation code
        }		
    } 	
    
    @Override
	public void onApplicationLoad() throws ClassNotFoundException {  
            
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
        
        try {  
            Global.getSettings().getScriptClassLoader().loadClass("org.dark.shaders.util.ShaderLib");  
        } catch (ClassNotFoundException ex) {  
            return;  
        }  
        ShaderLib.init();  
        LightData.readLightDataCSV("data/lights/diableavionics_bling.csv"); 
//        TextureData.readTextureDataCSV("data/lights/diableavionics_bump.csv"); 
    }	
	
    @Override
    public void onNewGame() {
        initDA();
    }

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case SCATTER_MISSILE_ID:
                return new PluginPick<MissileAIPlugin>(new diableavionics_ScatterMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case PD_MISSILE_ID:
                return new PluginPick<MissileAIPlugin>(new Diableavionics_antiMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case THUNDERBOLT_MISSILE_ID:
                return new PluginPick<MissileAIPlugin>(new diableavionics_ThunderboltMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case VALIANT_MISSILE_ID:
                return new PluginPick<MissileAIPlugin>(new diableavionics_swarmerMissileAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case BANISH_ID:
                return new PluginPick<MissileAIPlugin>(new Diableavionics_banishAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case THRUSH_ID:
                return new PluginPick<MissileAIPlugin>(new Diableavionics_thrushAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case SRAB_ID:
                return new PluginPick<MissileAIPlugin>(new diableavionics_SrabAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:        
        }
        return null;
    }		
}

