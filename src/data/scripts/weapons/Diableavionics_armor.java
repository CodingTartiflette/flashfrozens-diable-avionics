package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;

public class Diableavionics_armor implements EveryFrameWeaponEffectPlugin {
	
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (!weapon.getShip().isAlive()) return;
        
        ShipAPI ship=weapon.getShip();
        ShipAPI host=ship.getDroneSource();
        
        if (host!=null && host.isAlive()){
            ship.setFacing(host.getFacing());
            ship.getLocation().set(host.getLocation());
        }
    }
}
