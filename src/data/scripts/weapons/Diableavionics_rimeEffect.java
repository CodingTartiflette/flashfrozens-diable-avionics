package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;

public class Diableavionics_rimeEffect implements EveryFrameWeaponEffectPlugin {

    private boolean runOnce=false;
    private ShipAPI theShip;
    private ShipSystemAPI theSystem;
    private float timer=0;
    private final float TICK=0.5f;
    private final float DANGER_RANGE=1000;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
    
        if(!runOnce){
            runOnce=true;
            theShip=weapon.getShip();
            theSystem=theShip.getSystem();
        }
        
        if(engine.isPaused()){
            return;
        }
        
        if (theSystem.isOn() && !theShip.isRetreating()){   
            
            timer+=amount;
            
            if (timer>=TICK){
                timer-=TICK;
                
                CombatEntityAPI closest = AIUtils.getNearestEnemy(theShip);
                if(closest!=null && MathUtils.isWithinRange(closest, theShip, DANGER_RANGE)){
                    theShip.useSystem();
                    return;
                }
                
                List <MissileAPI> missiles=AIUtils.getNearbyEnemyMissiles(theShip, DANGER_RANGE);
                if(!missiles.isEmpty()){
                    float damage=0;
                    for(MissileAPI m : missiles){
                        damage+=m.getDamageAmount();                    
                    }                    
                    if (damage>=750){
                        theShip.useSystem();
                    }
                }
            }
        }
    } 
}