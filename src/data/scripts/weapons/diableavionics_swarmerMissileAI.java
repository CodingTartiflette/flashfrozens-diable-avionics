package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import java.util.Map;
import org.lwjgl.util.vector.Vector2f;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
//import data.scripts.weapons.diableavionics_swarmerTargets;

public class diableavionics_swarmerMissileAI implements MissileAIPlugin, GuidedMissileAI
{
    private CombatEngineAPI engine;
    private final MissileAPI missile;
    private CombatEntityAPI target;
    private boolean launch= true, isSeeking = true;
    private Vector2f lead = new Vector2f();
    private float eccm=3, seeking=0, timer=0, check=0.1f;
    //data
    private final float MAX_SPEED;
    private final float DAMPING = 0.1f;
    //time to complete a wave in seconds.
    private final float WAVE_TIME=2;
    //angle of the waving in degree (divided by 3 with ECCM)
    private final float WAVE_AMPLITUDE=15;
    private final float OVERSHOT, OFFSET, SEEKING_DONE;
    //range under which the missile start to get progressively more precise in game units.
    private float PRECISION_RANGE=200;
    
    public ShipAPI findRandomTargetWithinRange(MissileAPI missile) {
        int chooser;     
        Map<Integer, ShipAPI> TARGETLIST = ((diableavionics_swarmerTargets) missile.getWeapon().getEffectPlugin()).getMEMBERS();
        if (TARGETLIST.isEmpty()){
            return null;
        } else {
            chooser = (int)(Math.round( Math.random() * TARGETLIST.size()));
            return TARGETLIST.get(chooser);
        }
    }
    
    public diableavionics_swarmerMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        this.missile = missile;
        MAX_SPEED = missile.getMaxSpeed();
        if (missile.getSource().getVariant().getHullMods().contains("eccm")){
            eccm=1;
        }        
        //calculate the precision range factor
        PRECISION_RANGE=(float)Math.pow((2*PRECISION_RANGE),2);
        OFFSET=(float)(Math.random()*Math.PI*2);
        OVERSHOT=45-15*(1-eccm);
        SEEKING_DONE=0.25f+0.75f*(float)Math.random();
    }

    @Override
    public void advance(float amount) {        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }
        //skip AI if the missile is engineless or the game paused
        if (Global.getCombatEngine().isPaused()||missile.isFading() || missile.isFizzling()) {return;}     
        
        //The missile take some time before starting to seek it's target
        if (isSeeking) {
            seeking+=amount;
            if (seeking>= SEEKING_DONE) {
                isSeeking = false;
                setTarget(findRandomTargetWithinRange(missile));
                launch = true;  
            }
            return;
        }
        
        //if the missile has no target, pick the nearest one
        if ( target == null 
                || target.getOwner()==missile.getOwner()
                || (target instanceof ShipAPI && !((ShipAPI) target).isAlive()) 
                || !Global.getCombatEngine().isEntityInPlay(target)
                ){
            missile.giveCommand(ShipCommand.ACCELERATE);
            target = AIUtils.getNearestEnemy(missile);
            return;
        }
        
        timer+=amount;
        //finding lead point to aim to        
        if(launch || timer>=check){
            launch=false;
            timer -=check;
            //set the next check time
            check = Math.min(
                    0.25f,
                    Math.max(
                            0.025f,
                            MathUtils.getDistanceSquared(missile.getLocation(), target.getLocation())/PRECISION_RANGE)
            );
            lead = AIUtils.getBestInterceptPoint(
                    missile.getLocation(),
                    MAX_SPEED*eccm,
                    target.getLocation(),
                    target.getVelocity()
            );
            if (lead == null ) {
                lead = target.getLocation(); 
            }
        }
        
        //best angle for interception        
        float aimAngle = MathUtils.getShortestRotation(
                missile.getFacing(),
                VectorUtils.getAngle(
                        missile.getLocation(),
                        lead
                )
        );        
        
        //waving
        aimAngle+=WAVE_AMPLITUDE*4/3*check*eccm*Math.cos(OFFSET+missile.getElapsed()*(2*Math.PI/WAVE_TIME));
        
        if(Math.abs(aimAngle)<OVERSHOT){
            missile.giveCommand(ShipCommand.ACCELERATE);
        }
        if (aimAngle < 0) {
            missile.giveCommand(ShipCommand.TURN_RIGHT);
        } else {
            missile.giveCommand(ShipCommand.TURN_LEFT);
        }  
        
        // Damp angular velocity if the missile aim is getting close to the targeted angle
        if (Math.abs(aimAngle) < Math.abs(missile.getAngularVelocity()) * DAMPING) {
            missile.setAngularVelocity(aimAngle / DAMPING);
        }
    }
    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }
    @Override
    public void setTarget(CombatEntityAPI target) {
        this.target = target;
    }    
    public void init(CombatEngineAPI engine) {
    }
}
