package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.*;
import com.fs.starfarer.api.combat.DamageType;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class diableavionics_ScatterMissileAI implements MissileAIPlugin, GuidedMissileAI
{

    // Time until self destructing
    private final static float FUSE_TIME = 12.5f;
    // Our missile object
    private final MissileAPI missile;
    // Our current target (can be null)
    private CombatEntityAPI target;
    private float offtarget;
    private float baseofftarget;
	private float lifetimer;
    private IntervalUtil Timer = new IntervalUtil(0.1f, 0.25f);

    public static ShipAPI findBestTarget(MissileAPI missile)
    {
        ShipAPI source = missile.getSource();
        if (source != null && source.getShipTarget() != null && !source.getShipTarget().isHulk())
        {
            return source.getShipTarget();
        }

        ShipAPI closest = null;
        float distance, closestDistance = Float.MAX_VALUE;

		/**
        // Do not autonomously target fighters or drones
        for (ShipAPI tmp : AIUtils.getEnemiesOnMap(missile))
        {
            if (tmp.isFighter() || tmp.isDrone())
            {
                continue;
            }
            distance = MathUtils.getDistance(tmp, missile.getLocation());
            if (distance < closestDistance)
            {
                closest = tmp;
                closestDistance = distance;
            }
        }
		**/
        return closest;
    }

    public diableavionics_ScatterMissileAI(MissileAPI missile, ShipAPI launchingShip)
    {
	
	
        this.missile = missile;
        this.lifetimer = FUSE_TIME;		
		
        this.offtarget = 25f * (0.5f - (float) Math.random()); 		// Max initial off target (scatters missiles at launch)
        this.baseofftarget = 30f * (0.5f - (float) Math.random());  // Min off target (makes missiles stay scattered)

        // Support for 'fire at target by clicking on them' behavior
        List<ShipAPI> directTargets = CombatUtils.getShipsWithinRange(launchingShip.getMouseTarget(), 100f);
        if (!directTargets.isEmpty())
        {
            Collections.sort(directTargets, new CollectionUtils.SortEntitiesByDistance(launchingShip.getMouseTarget()));
            for (ShipAPI tmp : directTargets)
            {
                if (!tmp.isHulk() && tmp.getOwner() != launchingShip.getOwner())
                {
                    target=tmp;
                    break;
                }
            }
        }

        // Otherwise, use default Scatter targeting AI
        if (target == null)
        {
            target=findBestTarget(missile);
        }
    }

    @Override
    public void advance(float amount)
    {
	
        lifetimer -= amount;
        if (lifetimer <= 0f)
        {
            Global.getCombatEngine().applyDamage(missile, missile.getLocation(), missile.getHitpoints(), DamageType.OTHER, 0, true, true, missile);
            return;
        }
		
        // Apparently commands still work while fizzling
        if (missile.isFading() || missile.isFizzling())
        {
            return;
        }

        Timer.advance(amount);

        // If our current target is lost, assign a new one
        if (target == null || (target instanceof ShipAPI && ((ShipAPI) target).isHulk()) || (missile.getOwner() == target.getOwner()) || !Global.getCombatEngine().isEntityInPlay(target))
        {
            setTarget(findBestTarget(missile));
            return;
        }

        float angularDistance = MathUtils.getShortestRotation(missile.getFacing(), VectorUtils.getAngle(missile.getLocation(), target.getLocation()));

        if (Timer.intervalElapsed())
        {
            offtarget = (offtarget > 0 ? offtarget - 1 : offtarget + 1); // Reduce off target counter
        }

        float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());
        float offtargetby = (0f + (offtarget + (baseofftarget * target.getCollisionRadius() / 75f)));

        // Make it slightly more accurate as missile gets closer to target
        if (distance <= target.getCollisionRadius() * 2f)
        {
            offtargetby *= (distance - target.getCollisionRadius() * 1.55f) / target.getCollisionRadius() + 0.65f;
        }

        float AbsAngD = Math.abs(angularDistance - offtargetby);

        // Point towards target  
        if (AbsAngD > 0.5)
        {
            // Makes missile fly off target
            missile.giveCommand(angularDistance > offtargetby ? ShipCommand.TURN_LEFT : ShipCommand.TURN_RIGHT);
        }

        // Course correction
        if (AbsAngD < 5)
        {
            float MFlightAng = VectorUtils.getAngle(new Vector2f(0, 0), missile.getVelocity());
            float MFlightCC = MathUtils.getShortestRotation(missile.getFacing(), MFlightAng);
            if (Math.abs(MFlightCC) > 20)
            {
                missile.giveCommand(MFlightCC < 0 ? ShipCommand.STRAFE_LEFT : ShipCommand.STRAFE_RIGHT);
            }
        }

        // This missile should always be accelerating
        missile.giveCommand(ShipCommand.ACCELERATE);

        // Stop turning once missile is on target
        if (AbsAngD < 0.4)
        {
            missile.setAngularVelocity(0);
        }
    }

    @Override
    public CombatEntityAPI getTarget()
    {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI target)
    {
        this.target = target;
    }
}