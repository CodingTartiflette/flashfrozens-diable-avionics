package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;

public class diableavionics_HexafireEffectA implements OnHitEffectPlugin {
    
    private final float PITCH = 0.8f;
    private final float VOLUME = 0.3f;
    private String SOUND_ID = "diableavionics_crackle";
    
    private final String WEAPON_ID_B="diableavionics_hexafire_subB";
    private final String WEAPON_ID_C="diableavionics_hexafire_subC";

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, CombatEngineAPI engine) {
        if (shieldHit && !projectile.isFading()){
            
            String weapon_ID=WEAPON_ID_C;
            float range = MathUtils.getDistance(point, projectile.getWeapon().getLocation())/projectile.getWeapon().getRange();
            
            if (range < (1f/3f)){
                weapon_ID=WEAPON_ID_B;
            }
            
            float pointAngle = VectorUtils.getAngle(point, target.getShield().getLocation());
            if (Math.random()<(Math.abs(MathUtils.getShortestRotation(VectorUtils.getFacing(projectile.getVelocity()), pointAngle)/45)*0.8f+0.2f)){
                float bounceAngle = pointAngle + 180 + MathUtils.getShortestRotation(VectorUtils.getFacing(projectile.getVelocity()), pointAngle);

                engine.spawnProjectile(
                        projectile.getSource(),
                        projectile.getWeapon(),
                        weapon_ID,
                        point,
                        bounceAngle,
                        target.getVelocity()
                );
                Global.getSoundPlayer().playSound(SOUND_ID, PITCH*(float)((Math.random()*0.2f)+0.8f), VOLUME*(float)((Math.random()*0.2f)+0.8f),target.getLocation(), target.getVelocity());
            }
        }
    }
}