// By Tartiflette: a target designation system that work in concordance with the swarmer missile AI. Pick random missile targets around the ship target, or around the ship if there isn't any target selected.
package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.util.HashMap;
import java.util.Map;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class diableavionics_swarmerTargets implements EveryFrameWeaponEffectPlugin {

    private boolean hasFired = false;
    private int searchRange = 600;
    public Map< Integer , ShipAPI > MEMBERS = new HashMap<>();
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
    
        if( weapon.isFiring() && !hasFired)
        {
            ShipAPI source = weapon.getShip();
            ShipAPI shipTarget = source.getShipTarget();
            getMEMBERS().clear();
            int nbKey = 0;
            Vector2f epicenter;
            //center the target search zone ont the ship target if any, or then on the ship itself
            if (shipTarget != null && !shipTarget.isHulk()) {
                epicenter = shipTarget.getLocation();
                searchRange = 600;
            } else {
                epicenter = source.getLocation();
                searchRange = 600;
            }
            //store all enemy ships in the search zone
            for (ShipAPI tmp : CombatUtils.getShipsWithinRange(epicenter, searchRange)) {
                if (tmp != null && tmp.getOwner() != source.getOwner()) {
                    getMEMBERS().put(nbKey, tmp);
                    nbKey++;
                }
            }
            hasFired = true;
        }
        if(weapon.getChargeLevel()==0) {hasFired = false;}        
    }    
    /**
     * @return the MEMBERS
     */
    public Map< Integer , ShipAPI > getMEMBERS() {
        return MEMBERS;
    }
}