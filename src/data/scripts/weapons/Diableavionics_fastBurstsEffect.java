package data.scripts.weapons;


import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class Diableavionics_fastBurstsEffect implements BeamEffectPlugin
{
    private boolean hasFired=false;
    private float width, timer=0, count=0;
    private String id;
    
    private final String IDa="diableavionics_glaux_firing";
    private final float WIDTHa = 22;
    private final String IDb="diableavionics_burchel_firing";
    private final float WIDTHb = 16;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {
        // Don't bother with any checks if the game is paused
        if (engine.isPaused()) {
            return;
        }
        
        if (beam.getWeapon().getId().equals("diableavionics_glaux")){
            width=WIDTHa;
            id=IDa;
        } else {
            width=WIDTHb;
            id=IDb;
        }
        
        if(beam.getBrightness()==1 && count<6) {            
            Vector2f start = beam.getFrom();
            Vector2f end = beam.getTo();
            
            if (MathUtils.getDistanceSquared(start, end)==0){
                return;
            }
            
            timer+=amount;
            if (timer>=0.1f){
                timer-=0.1f;
                hasFired=false;
            }
            
            float theWidth = width * ( 0.5f * (float) Math.cos( 20*Math.PI * Math.min(timer,0.05f) ) + 0.5f ) ;
            beam.setWidth(theWidth);
                        
            if (!hasFired){   
                hasFired=true; 
                count++;
                //play sound (to avoid limitations with the way weapon sounds are handled)
                Global.getSoundPlayer().playSound(id, 1f, 1f, start, beam.getSource().getVelocity());

            }
        }
        if(beam.getWeapon().getChargeLevel()==0)
        {
            hasFired=false;
            timer = 0;
            count=0;
        }
    }
}