//By Tartiflette, fast and highly customizable Missile AI.
package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import java.util.Collections;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class Diableavionics_thrushAI implements MissileAIPlugin, GuidedMissileAI {
          
    
    //////////////////////
    //     SETTINGS     //
    //////////////////////
    
    //Angle with the target beyond which the missile turn around without accelerating. Avoid endless circling.
    //  Set to a negative value to disable
    private final float OVERSHOT_ANGLE=60;
    
    //Damping of the turn speed when closing on the desired aim. The smaller the snappier.
    private final float DAMPING=0.1f;
    
    //Does the missile switch its target if it has been destroyed?
    private final boolean TARGET_SWITCH=true;
    
    //range in which the missile seek a target in game units.
    private final float MAX_SEARCH_RANGE = 5000;
    
    //range under which the missile start to get progressively more precise in game units.
    private float PRECISION_RANGE=1000;
    
    //Is the missile lead the target or tailchase it?
    private final boolean LEADING=true;
    
    //Leading loss without ECCM hullmod. The higher, the less accurate the leading calculation will be.
    //   1: perfect leading with and without ECCM
    //   2: half precision without ECCM
    //   3: a third as precise without ECCM. Default
    //   4, 5, 6 etc : 1/4th, 1/5th, 1/6th etc precision.
    private float ECCM=2;   //A VALUE BELOW 1 WILL PREVENT THE MISSILE FROM EVER HITTING ITS TARGET!
    
    
    //////////////////////
    //    VARIABLES     //
    //////////////////////
    
    //max speed of the missile after modifiers.
    private final float MAX_SPEED;
    private CombatEngineAPI engine;
    private final MissileAPI MISSILE;
    private CombatEntityAPI target;
    private Vector2f lead = new Vector2f();
    private boolean launch=true;
    private float timer=0, check=0f;

    //////////////////////
    //  DATA COLLECTING //
    //////////////////////
    
    public Diableavionics_thrushAI(MissileAPI missile, ShipAPI launchingShip) {
        this.MISSILE = missile;
        MAX_SPEED = missile.getMaxSpeed();
        if (missile.getSource().getVariant().getHullMods().contains("eccm")){
            ECCM=1;
        }        
        //calculate the precision range factor
        PRECISION_RANGE=(float)Math.pow((2*PRECISION_RANGE),2);
    }
    
    //////////////////////
    //   MAIN AI LOOP   //
    //////////////////////
    
    @Override
    public void advance(float amount) {
        
        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }
        
        //skip the AI if the game is paused, the missile is engineless or fading
        if (Global.getCombatEngine().isPaused() || MISSILE.isFading()) {return;}
        
        //assigning a target if there is none or it got destroyed
        if (target == null
                || target.getOwner()==MISSILE.getOwner()
                || (TARGET_SWITCH && (target instanceof ShipAPI && ((ShipAPI) target).isHulk())
                                  || !engine.isEntityInPlay(target)
                   )
                ){
            setTarget(assignTarget(MISSILE));
            //forced acceleration by default
            MISSILE.giveCommand(ShipCommand.ACCELERATE);
            return;
        }
        
        timer+=amount;
        //finding lead point to aim to        
        if(launch || timer>=check){
            launch=false;
            timer -=check;
            //set the next check time
            check = Math.min(
                    0.25f,
                    Math.max(
                            0.03f,
                            MathUtils.getDistanceSquared(MISSILE.getLocation(), target.getLocation())/PRECISION_RANGE)
            );
            if(LEADING){
                //best intercepting point
                lead = AIUtils.getBestInterceptPoint(
                        MISSILE.getLocation(),
                        MAX_SPEED*ECCM, //if eccm is intalled the point is accurate, otherwise it's placed closer to the target (almost tailchasing)
                        target.getLocation(),
                        target.getVelocity()
                );                
                //null pointer protection
                if (lead == null) {
                    lead = target.getLocation(); 
                }
            } else {
                lead = target.getLocation();
            }
            
            if((MISSILE.getFlightTime()>1 
                    && MathUtils.getDistanceSquared(MISSILE, lead)<1000000
                    && Math.abs( MathUtils.getShortestRotation(VectorUtils.getFacing(MISSILE.getVelocity()), VectorUtils.getAngle(MISSILE.getLocation(), lead)))>110
                    && MISSILE.getVelocity().lengthSquared()>Math.pow(MISSILE.getMaxSpeed(),2)/2)
                    || MISSILE.isFizzling()
                    ){
                for(float i=0; i<=3; i++){
                    engine.spawnProjectile(
                            MISSILE.getSource(),
                            MISSILE.getWeapon(),
                            "diableavionics_micromissile",
                            MISSILE.getLocation(),
                            MISSILE.getFacing() - 90 + 45*i,
                            MISSILE.getVelocity()
                    );
                }                
                engine.applyDamage(
                        MISSILE,
                        MISSILE.getLocation(),
                        MISSILE.getHitpoints()*2,
                        DamageType.FRAGMENTATION,
                        0,
                        true,
                        false,
                        MISSILE.getSource()
                );
            }            
        }
        
        //best velocity vector angle for interception
        float correctAngle = VectorUtils.getAngle(
                        MISSILE.getLocation(),
                        lead
                );
        
        //velocity angle correction
        float offCourseAngle = MathUtils.getShortestRotation(
                VectorUtils.getFacing(MISSILE.getVelocity()),
                correctAngle
                );

        float correction = MathUtils.getShortestRotation(                
                correctAngle,
                VectorUtils.getFacing(MISSILE.getVelocity())+180
                ) 
                * 0.5f * //oversteer
                (float)((Math.sin(Math.PI/90*(Math.min(Math.abs(offCourseAngle),45))))); //damping when the correction isn't important

        //modified optimal facing to correct the velocity vector angle as soon as possible
        correctAngle = correctAngle+correction;
        
        //target angle for interception        
        float aimAngle = MathUtils.getShortestRotation( MISSILE.getFacing(), correctAngle);
        
        if(OVERSHOT_ANGLE<=0 || Math.abs(aimAngle)<OVERSHOT_ANGLE){
            MISSILE.giveCommand(ShipCommand.ACCELERATE);  
        }
        
        if (aimAngle < 0) {
            MISSILE.giveCommand(ShipCommand.TURN_RIGHT);
        } else {
            MISSILE.giveCommand(ShipCommand.TURN_LEFT);
        }  
        
        // Damp angular velocity if the missile aim is getting close to the targeted angle
        if (Math.abs(aimAngle) < Math.abs(MISSILE.getAngularVelocity()) * DAMPING) {
            MISSILE.setAngularVelocity(aimAngle / DAMPING);
        }
    }
    
    //////////////////////
    //    TARGETING     //
    //////////////////////
    
    public CombatEntityAPI assignTarget(MissileAPI missile){
        
        ShipAPI theTarget=null;        
        ShipAPI source = missile.getSource();        
        ShipAPI currentTarget;
        
        //check for a target from its source
        if(source != null
                && source.getShipTarget() != null
                && source.getShipTarget() instanceof ShipAPI
                && source.getShipTarget().getOwner() != missile.getOwner()
                ){
            currentTarget=source.getShipTarget();
        } else {
            currentTarget=null;
        }
        
        if(source!=null){
            //ship target first
            if(currentTarget!=null
                    && currentTarget.isAlive()
                    && currentTarget.getOwner()!=missile.getOwner()
                    && !((currentTarget.isDrone() || currentTarget.isFighter()))
                    ){
                theTarget=currentTarget;                
            } else {
                //or cursor target if there isn't one
                List<ShipAPI> mouseTargets = CombatUtils.getShipsWithinRange(source.getMouseTarget(), 100f);
                if (!mouseTargets.isEmpty()) {
                    Collections.sort(mouseTargets, new CollectionUtils.SortEntitiesByDistance(source.getMouseTarget()));
                    for (ShipAPI tmp : mouseTargets) {
                        if (tmp.isAlive() 
                                && tmp.getOwner() != missile.getOwner()
                                && !((tmp.isDrone() || tmp.isFighter() || tmp.isFrigate()))
                                ) {
                            theTarget=tmp;
                            break;
                        }
                    }
                }                
            }
        }
        //still no valid target? lets try the closest one
        //most of the time a ship will have a target so that doesn't need to be perfect.
        if(theTarget==null){
            List<ShipAPI> closeTargets = AIUtils.getNearbyEnemies(missile, MAX_SEARCH_RANGE);
            if (!closeTargets.isEmpty()) {
                Collections.sort(closeTargets, new CollectionUtils.SortEntitiesByDistance(missile.getLocation()));
                for (ShipAPI tmp : closeTargets) {
                    if (tmp.isAlive() 
                            && tmp.getOwner() != missile.getOwner()
                            ) {
                        if (tmp.isCapital() || tmp.isCruiser()){
                            theTarget=tmp;
                            break;
                        } else if (tmp.isDestroyer() && Math.random()>0.5){
                            theTarget=tmp;
                            break;
                        } else if (tmp.isDestroyer() && Math.random()>0.75){
                            theTarget=tmp;
                            break;
                        } else if (!tmp.isDrone() && !tmp.isFighter() && Math.random()>0.95){
                            theTarget=tmp;
                            break;
                        }
                    }
                }                
            } 
        }
                
        return theTarget;
    }
    
    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI target) {
        this.target = target;
    }
    
    public void init(CombatEngineAPI engine) {}
}