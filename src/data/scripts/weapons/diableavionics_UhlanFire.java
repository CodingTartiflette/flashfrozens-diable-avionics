package data.scripts.weapons;

import com.fs.starfarer.api.AnimationAPI;
//import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
//import com.fs.starfarer.api.graphics.SpriteAPI;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;

public class diableavionics_UhlanFire implements EveryFrameWeaponEffectPlugin {
        
    private boolean runOnce=false, invisible=false;
//    private SpriteAPI barrel;
//    private float barrelHeight=0, recoil=0;
//    private final float maxRecoil=10;
    private AnimationAPI barrel;
    private float recoil=0, maxFrame=0, motion=1, ammo=0;
    
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(engine.isPaused() || weapon.getShip().getOriginalOwner()==-1 || invisible){return;}
        
//        if(!runOnce){
//            runOnce=true;
//            barrel=weapon.getBarrelSpriteAPI();
//            barrelHeight=barrel.getHeight()/2;
//            return;
//        }
//        
//        if(weapon.getChargeLevel()==1){
//            recoil=Math.min(1, recoil+0.33f);
//            engine.addHitParticle(
//                    MathUtils.getPointOnCircumference(weapon.getLocation(), (20-recoil*maxRecoil), weapon.getCurrAngle()),
//                    weapon.getShip().getVelocity(),
//                    150,
//                    1,
//                    0.5f,
//                    Color.red
//            );
//        } else {
//            recoil=Math.max(0, recoil-(0.05f*amount));
//        }
//        barrel.setCenterY(barrelHeight-(recoil*maxRecoil));
//        
//        if(sound && weapon.getChargeLevel()>0){
//            Global.getSoundPlayer().playSound("diableavionics_uhlan_chargeup", 1, 1, weapon.getLocation(), weapon.getShip().getVelocity());
//            sound=false;
//        } else if (weapon.getChargeLevel()==0 && recoil==0){
//            sound=true;
//        }
        
        if(!runOnce){
            if(weapon.getSlot().isHidden()){
                invisible=true;
                return;
            }
            runOnce=true;
            barrel=weapon.getAnimation();
            maxFrame=barrel.getNumFrames();
            ammo=weapon.getAmmo();
            return;
        }

        if(weapon.getChargeLevel()==1 && weapon.getAmmo()<ammo){

            recoil=Math.min(0.85f, recoil+0.33f);
            motion=0.25f;
            engine.addHitParticle(
                    MathUtils.getPointOnCircumference(weapon.getLocation(), (50-(recoil*maxFrame)), weapon.getCurrAngle()),
                    weapon.getShip().getVelocity(),
                    50,
                    0.5f,
                    0.1f,
                    Color.white
            );
            engine.addHitParticle(
                    MathUtils.getPointOnCircumference(weapon.getLocation(), (50-(recoil*maxFrame)), weapon.getCurrAngle()),
                    weapon.getShip().getVelocity(),
                    75,
                    0.75f,
                    0.25f,
                    Color.yellow
            );
            engine.addHitParticle(
                    MathUtils.getPointOnCircumference(weapon.getLocation(), (50-(recoil*maxFrame)), weapon.getCurrAngle()),
                    weapon.getShip().getVelocity(),
                    100,
                    1f,
                    0.5f,
                    Color.red
            );            
        } else if (recoil>0){

            motion=Math.min(1,motion+(amount*0.75f));            
            float effectiveMotion=(float)-Math.cos(motion*Math.PI);            
            recoil=Math.min(1, Math.max(0, recoil-(effectiveMotion*1.5f*amount)));

        } else {

            recoil=0;
            motion=1;

        }

        ammo=weapon.getAmmo();        
        int frame = Math.round((Math.max(0, Math.min(1, recoil)))*(maxFrame-1));
        barrel.setFrame(frame);
    }
}