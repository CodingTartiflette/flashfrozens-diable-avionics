
package data.scripts.weapons;

//import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.AnimationAPI;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import data.scripts.plugins.SpriteRenderManager;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class Diableavionics_derechoEffect implements EveryFrameWeaponEffectPlugin {

    private boolean runOnce=false, animIsOn=true;
    private ShipAPI theShip;
    private ShipSystemAPI theSystem;
    private AnimationAPI theAnim;
    private float timer=0, animation=0, fade=0, sparkle=0, rim=0, axis=0;
    private final float TICK=0.1f, ANIM=0.05f, RANGE=2000, RIMTICK=0.5f;
    private int LENGTH, frame=0;
    private List<MissileAPI> disabled= new ArrayList<>();
    private List<MissileAPI> toRemove= new ArrayList<>();
    public float rangeMult=0;
        
    private final String zapSprite="zap_0";
    private final int zapFrames=8;    
    
    private final String stripeSprite="areaStripes";
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
    
        if(!runOnce){
            runOnce=true;
            theShip=weapon.getShip();
            theSystem=theShip.getSystem();
            LENGTH=weapon.getAnimation().getNumFrames()-1;
            theAnim=weapon.getAnimation();
            disabled.clear();
            toRemove.clear();       
        }
        
        if(engine.isPaused()){
            return;
        }
        
        if (theSystem.isOn()){   
            if (!animIsOn){
                engine.addSmoothParticle(weapon.getLocation(), weapon.getShip().getVelocity(), 3000, 1f, 0.5f, new Color(0.1f,0f,0.15f));
            }
            timer+=amount;
            animIsOn=true;
            
            if (timer>=TICK){
                timer-=TICK;
                
                List <MissileAPI> missiles=AIUtils.getNearbyEnemyMissiles(theShip, RANGE*rangeMult);
                for (MissileAPI m : missiles){
                    if(!disabled.contains(m)){
                        disabled.add(m);
                    }
                }          
            }            
            lockMissiles(engine,amount);  
            
            //AURA
            rim+=amount;
            if(rim>RIMTICK){
                rim=0;
                SpriteRenderManager.objectspaceRender(
                                        Global.getSettings().getSprite("fx",stripeSprite),
                                        theShip,
                                        new Vector2f(),
                                        null,
                                        new Vector2f(1160,4000),
                                        new Vector2f(),
                                        axis,
                                        +10f,
                                        false,
                                        new Color(0.5f,0.5f,0.5f),
                                        true,
                                        1f,
                                        0f,
                                        1f,
                                        true
                                );                
                axis+=45;
            }
            
        } else {
            timer=0;
            animIsOn=false;
        }       
        
                
        if (animIsOn || fade>0){            
            animation+=amount;            
            if(animation>ANIM){
                animation-=ANIM;                   
                
                frame++;
                if(frame>LENGTH){
                    frame=1;
                }
                theAnim.setFrame(frame);
                
                if(animIsOn){
                    fade = Math.min(fade+0.02f,1);
                } else {
                    fade = Math.max(fade-0.02f,0);
                }
                theAnim.setAlphaMult(fade);
            }
        }
    } 
    
    private void lockMissiles(CombatEngineAPI engine, float amount){
        
        boolean sparkling = false;
        sparkle+=amount;
        if (sparkle>TICK){
            sparkle-=TICK;
            sparkling=true;
        }
        
        if(!disabled.isEmpty()){
            toRemove.clear();
            for (MissileAPI m : disabled){
                if(m.isFading() || m.didDamage() || !engine.isEntityInPlay(m)){
                    toRemove.add(m);
                } else {
                    m.setAngularVelocity(0);
                    m.setFlightTime(m.getFlightTime()+amount);
                    if(sparkling){               
                        //flameout
                        if(Math.random()>0.98){
                            m.flameOut();
                            engine.addHitParticle(
                                    m.getLocation(),
                                    m.getVelocity(),
                                    m.getCollisionRadius()+25*(float)Math.random(),
                                    1f,
                                    0.5f+0.5f*(float)Math.random(),
                                    Color.PINK
                            );
                        }
                        
                        //zaps
                        if(Math.random()>0.8){
                            int chooser = new Random().nextInt(zapFrames - 1) + 1;
                            float rand = 0.5f*(float)Math.random()+0.5f;
                            
                            SpriteRenderManager.objectspaceRender(
                                    Global.getSettings().getSprite("fx",zapSprite+chooser),
                                    m,
                                    new Vector2f(),
                                    null,
                                    new Vector2f(48*rand,48*rand),
                                    new Vector2f((float)Math.random()*20,(float)Math.random()*20),
                                    (float)Math.random()*360,
                                    (float)(Math.random()-0.5f)*10,
                                    false,
                                    new Color(255,175,255),
                                    true,
                                    0,
                                    0.1f+(float)Math.random()*0.1f,
                                    0.1f,
                                    false
                            );
                        }
                    }
                }
            }
            
            if (!toRemove.isEmpty()){                
                for (MissileAPI m : toRemove){
                    disabled.remove(m);
                }
            }
            
        }
    }
    
    public float getRangeMult() {
        return rangeMult;
    }       
    
    public void setRangeMult(float mult) {
        this.rangeMult = mult;
    }
}