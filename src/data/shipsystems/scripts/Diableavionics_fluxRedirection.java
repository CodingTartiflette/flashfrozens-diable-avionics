package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class Diableavionics_fluxRedirection implements ShipSystemStatsScript {

	public static final float ROF_BONUS_PERCENT = 50;
	public static final float DAMAGE_BONUS_PERCENT = 50;
	public static final float FLUX_REDUCTION = 25f;
	
        @Override
	public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
		
		float bonusPercent = DAMAGE_BONUS_PERCENT * effectLevel;
		stats.getBeamWeaponDamageMult().modifyPercent(id, bonusPercent);                
                
		float mult = ROF_BONUS_PERCENT * effectLevel;
		stats.getBallisticRoFMult().modifyPercent(id, mult);
		stats.getEnergyRoFMult().modifyPercent(id, mult);
		stats.getBallisticWeaponFluxCostMod().modifyPercent(id, -FLUX_REDUCTION* effectLevel);
                stats.getEnergyWeaponFluxCostMod().modifyPercent(id, -FLUX_REDUCTION* effectLevel);
                stats.getShieldDamageTakenMult().modifyMult(id, 1+(0.5f*effectLevel));
                
	}
        
        @Override
	public void unapply(MutableShipStatsAPI stats, String id) {
		stats.getBallisticRoFMult().unmodify(id);
		stats.getBallisticWeaponFluxCostMod().unmodify(id);
		stats.getEnergyRoFMult().unmodify(id);
		stats.getEnergyWeaponFluxCostMod().unmodify(id);
		stats.getBeamWeaponDamageMult().unmodify(id);
                stats.getShieldDamageTakenMult().unmodify(id);
	}
	
        @Override
	public StatusData getStatusData(int index, State state, float effectLevel) {
		int mult = Math.round(ROF_BONUS_PERCENT * effectLevel);
		int flux = Math.round(FLUX_REDUCTION * effectLevel);
		if (index == 0) {
			return new StatusData("Weapons damage output +" + mult + "%", false);
		}
		if (index == 1) {
			return new StatusData("Shield damage taken +" + mult+ "%", false);
		}
		if (index == 2) {
			return new StatusData("Weapons flux use -" + flux + "%", false);
		}
		return null;
	}
}
