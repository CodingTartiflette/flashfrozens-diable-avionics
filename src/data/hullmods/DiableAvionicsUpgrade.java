package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import java.awt.Color;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.lazywizard.lazylib.MathUtils;

public class DiableAvionicsUpgrade extends BaseHullMod {

    public static final float SHIELD_BONUS_UNFOLD = 200f;

    private static class BuffData {
        float level;
        float autoaim;
        float rangeBuff;
        float rangeDebuff;
        float rangethreshold;
        float turnrate;

        public BuffData(float level, float autoaim, float rangeAdd, float rangeSub, float rangethreshold, float turnrate) {
            this.level = level;
            this.autoaim = autoaim;
            this.rangeBuff = rangeAdd;
            this.rangeDebuff = rangeSub;
            this.rangethreshold = rangethreshold;
            this.turnrate = turnrate;
        }
    }    
    private static Map<ShipAPI.HullSize, BuffData> weaponBuff = new HashMap<>();
    static {
        weaponBuff.put(ShipAPI.HullSize.DEFAULT, new BuffData(2f, 15, 150, 10, 1500, 15));
        weaponBuff.put(ShipAPI.HullSize.CAPITAL_SHIP, new BuffData(1f/2f, 15, 300, 10, 3000, 15));
        weaponBuff.put(ShipAPI.HullSize.CRUISER, new BuffData(2f/3f, 15, 250, 10, 2500, 15));
        weaponBuff.put(ShipAPI.HullSize.DESTROYER, new BuffData(1f, 15, 200, 10, 2000, 15));
        weaponBuff.put(ShipAPI.HullSize.FRIGATE, new BuffData(2f, 15, 150, 10, 1500, 15));
        weaponBuff.put(ShipAPI.HullSize.FIGHTER, new BuffData(0f, 0, 0, 0, 0, 0));
    }
    private String ID="Target Analysis",ERROR="IncompatibleHullmodWarning";
    
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>();
    static
    {
        // These hullmods will automatically be removed
        // This prevents unexplained hullmod blocking
        BLOCKED_HULLMODS.add("safetyoverrides");
    }
            
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        //unfold rate
        stats.getShieldUnfoldRateMult().modifyPercent(id, SHIELD_BONUS_UNFOLD);	
    }
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id){
        
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {                
                ship.getVariant().removeMod(tmp);      
                ship.getVariant().addMod(ERROR);
            }
        }
    }        
    
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        
        int tick = (int)ship.getTimeDeployedForCRReduction()/60;
        float effectLevel = Math.min(1, -1 + (weaponBuff.get(ship.getHullSize()).level)*tick);
        float rangeEffect = Math.min(1,0.5f*(weaponBuff.get(ship.getHullSize()).level)*tick);
        
        ship.getMutableStats().getWeaponTurnRateBonus().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).turnrate*effectLevel/100));
        ship.getMutableStats().getAutofireAimAccuracy().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).autoaim*effectLevel/100));
        
//        ship.getMutableStats().getWeaponRangeThreshold().modifyFlat(ID, (weaponBuff.get(ship.getHullSize()).rangethreshold));
//        
//        ship.getMutableStats().getWeaponRangeMultPastThreshold().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).rangeDebuff*rangeEffect/100));
//        ship.getMutableStats().getBallisticWeaponRangeBonus().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).rangeBuff*rangeEffect/100));
//        ship.getMutableStats().getEnergyWeaponRangeBonus().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).rangeBuff*rangeEffect/100));
//        ship.getMutableStats().getBeamWeaponRangeBonus().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).rangeBuff*rangeEffect/100));
                
        ship.getMutableStats().getWeaponRangeThreshold().modifyFlat(ID, (weaponBuff.get(ship.getHullSize()).rangethreshold));
        
        ship.getMutableStats().getWeaponRangeMultPastThreshold().modifyMult(ID, 1/(1-weaponBuff.get(ship.getHullSize()).rangeDebuff));
        
        ship.getMutableStats().getBallisticWeaponRangeBonus().modifyFlat(ID+"2", weaponBuff.get(ship.getHullSize()).rangeBuff*rangeEffect);
        ship.getMutableStats().getEnergyWeaponRangeBonus().modifyFlat(ID+"2", weaponBuff.get(ship.getHullSize()).rangeBuff*rangeEffect);
        ship.getMutableStats().getBeamWeaponRangeBonus().modifyFlat(ID+"2", weaponBuff.get(ship.getHullSize()).rangeBuff*rangeEffect);
        
        ship.getMutableStats().getBallisticWeaponRangeBonus().modifyMult(ID, 1-(weaponBuff.get(ship.getHullSize()).rangeDebuff*rangeEffect/100));
        ship.getMutableStats().getEnergyWeaponRangeBonus().modifyMult(ID, 1-(weaponBuff.get(ship.getHullSize()).rangeDebuff*rangeEffect/100));
        ship.getMutableStats().getBeamWeaponRangeBonus().modifyMult(ID, 1-(weaponBuff.get(ship.getHullSize()).rangeDebuff*rangeEffect/100));
        
        ship.getMutableStats().getRecoilDecayMult().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).autoaim*effectLevel/100));
        ship.getMutableStats().getRecoilPerShotMult().modifyMult(ID, 1+(weaponBuff.get(ship.getHullSize()).autoaim*effectLevel/100));   
        
        EnumSet WEAPON_TYPES = EnumSet.of(WeaponType.BALLISTIC,WeaponType.ENERGY);
        ship.setWeaponGlow(
                (effectLevel+1)*0.5f,
                new Color(0.25f*(effectLevel+1)+0.25f,0.4f*(effectLevel+1)+0.1f,0.05f*(effectLevel+1)+0.05f),
                WEAPON_TYPES
        );
        
        ShipAPI playerShip = Global.getCombatEngine().getPlayerShip();
        if(ship==playerShip){
            Global.getCombatEngine().maintainStatusForPlayerShip(
                    "AdvancedAvionicsBoost",
                    "graphics/icons/hullsys/high_energy_focus.png",
                    "Patterns Analysis",                        
                    Math.round(effectLevel*15)+"% Targetting improvement",
                    effectLevel < 0f);
        }
    }
	
    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "-15%";
        }
        if (index == 1) {
            return "60 seconds";
        }
        if (index == 2) {
            return "60/120/180/240";
        }
        if (index == 3) {
            return "+15%";
        }
        if (index == 4) {
            return "150/200/250/300";
        }
        if (index == 5) {
            return "Safety Override";
        }
        
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a diableavionics hull id
        return ( ship.getHullSpec().getHullId().startsWith("diableavionics_"));	
    }
}
