package data.hullmods;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class DiableAvionicsUniversalDecksUpgrade extends BaseHullMod {

    //private static final List allowedIds = new ArrayList();
	

    public static final float REFIT_BONUS = 30f;
	
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {

            //recreates fighters faster
    stats.getFighterRefitTimeMult().modifyPercent(id, -REFIT_BONUS);

            }
    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "-" + (int) REFIT_BONUS + "%";
        }
            return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship)
    {
        // Allows any ship with a diableavionics hull id
        return ( ship.getHullSpec().getHullId().startsWith("diableavionics_"));	
    }
}
