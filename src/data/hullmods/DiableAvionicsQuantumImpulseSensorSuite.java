package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class DiableAvionicsQuantumImpulseSensorSuite extends BaseHullMod {

    private static final float SENSOR_INCREASE = 250f;		
    private static final float PROFILE_INCREASE = 50f;	
	
    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getSensorStrength().modifyPercent(id, SENSOR_INCREASE);
        stats.getSensorProfile().modifyPercent(id, PROFILE_INCREASE);
    }
	
    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) return "" + (int) SENSOR_INCREASE;
        if (index == 1) return "" + (int) PROFILE_INCREASE;
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship)
    {
        // Allows any ship with a diableavionics hull id
        return ( ship.getHullSpec().getHullId().startsWith("diableavionics_"));	
    }
	
}
